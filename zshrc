export PATH="$HOME/.local/usr/bin:$HOME/.local/bin:/opt/X11/bin:/usr/local/opt/coreutils/libexec/gnubin:/usr/local/MacGPG2/bin:/usr/texbin:/usr/local/bin/pear:/Developer/NVIDIA/CUDA-5.5/bin:/Library/Frameworks/Python.framework/Versions/2.7/bin:/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:"

export EDITOR="emacsclient -c"
export ALTERNATE_EDITOR="nano"
export LANG=en_US.UTF-8
export LC_ALL=en_US.UTF-8
export TERM="xterm-256color"
export omg_ungit_prompt="%~ • "
ZSH_HIGHLIGHT_HIGHLIGHTERS=(main brackets pattern cursor root)
ZSH_TMUX_AUTOSTART=true
ZSH_TMUX_AUTOCONNECT=false
ZSH_TMUX_FIXTERM_WITH_256COLOR="screen-256color"

alias emacs="emacsclient -c -n"
alias sudo="nocorrect sudo"
alias ls="ls --color=auto"

what-the-commit() {
    git commit -m "$(curl --silent http://whatthecommit.com/index.txt)"
}

eval `dircolors`
source /usr/local/share/zsh/site-functions/_aws
[[ -s "$HOME/.rvm/scripts/rvm" ]] && source "$HOME/.rvm/scripts/rvm" # Load RVM into a shell session *as a function*
source "$HOME/.antigen/antigen.zsh"

antigen use oh-my-zsh
antigen bundle tmux
antigen apply

antigen bundles <<EOF
   aws
   bower
   composer
   common-aliases
   gpg-agent
   git
   git-extras
   osx
   ssh-agent
   sudo
   symfony2
   terminalapp
   vagrant
   zsh-users/zsh-syntax-highlighting
   zsh-users/zsh-history-substring-search
   zsh-users/zsh-completions
   zsh-users/zaw
EOF
antigen bundle powerline/powerline powerline/bindings/zsh

if [ -n "$INSIDE_EMACS" ]; then
    export ZSH_TMUX_AUTOSTARTED=true
    chpwd() { print -P "\033AnSiTc %d" }
    print -P "\033AnSiTu %n"
    print -P "\033AnSiTc %d"
fi
