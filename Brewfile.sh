#!/bin/bash

brew tap homebrew/dupes
brew tap homebrew/php
brew tap homebrew/boneyard
brew tap homebrew/x11
brew tap beeftornado/rmtree
brew tap caskroom/cask
brew tap railwaycat/emacsmacport

brew install aspell
brew install auctex
brew install awscli
brew install bison
brew install boost
brew install brew-cask
brew install brew-rmtree
brew install cmake
brew install coreutils
brew install docker
brew install emacs-mac
brew install expat
brew install findutils
brew install fig
brew install fontconfig
brew install gawk
brew install ghostscript
brew install git
brew install gnu-getopt
brew install gnu-tar
brew install gnutls
brew install icu4c
brew install imagemagick
brew install libiconv
brew install libsodium
brew install libxslt
brew install lua
brew install markdown
brew install maven
brew install mhash
brew install mosh
brew install nmap
brew install node
brew install offline-imap
brew install openssh
brew install pandoc
brew install phpunit
brew install python
brew install python3
brew install re2c
brew install ruby
brew install sane-backends
brew install sshfs
brew install subversion
brew install tmux
brew install tor
brew install wget
brew install zopfli
brew install zsh
brew install zsh-completions

brew cask install arq
brew cask install bitcoin-core
brew cask install chromecast
brew cask install dnscrypt
brew cask install dropbox
brew cask install firefox
brew cask install flux
brew cask install garmin-ant-agent
brew cask install garmin-communicator
brew cask install garmin-express
brew cask install garmin-training-center
brew cask install gnucash
brew cask install google-drive
brew cask install google-hangouts
brew cask install handbrake
brew cask install jitsi
brew cask install mpv
brew cask install qtox
brew cask install slack
brew cask install sublime-text
brew cask install synergy
brew cask install teamspeak-client
brew cask install thunderbird
brew cask install transmission
brew cask install tunnelblick
brew cask install wireshark
